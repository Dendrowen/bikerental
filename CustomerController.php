<?php

class CustomerController{

    static function index($get, $post){
        return DB::select('SELECT * FROM `customers`', []);
    }

    static function show($get, $post){
        return DB::selectCascade('customers', $get['customer_id']);
        // return DB::select('SELECT * FROM `customers` WHERE `id` = :customer_id', $get);
    }

    static function create($get, $post){
        if(DB::insert('customers', $post)){
            http_response_code(201);
            return 'success';
        } else {
            http_response_code(406);
            return 'failed';
        }

    }

    static function update($get, $post){
        $post->id = $get['customer_id'];
        if(DB::update('customers', $post)){
            http_response_code(202);
            return 'success';
        } else {
            http_response_code(406);
            return 'failed'; 
        }
    }

}