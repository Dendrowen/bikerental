<?php

require_once 'tools.php';
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

route('GET',    'bikes', 
'BikeController::index');


route('GET',    'stores', 
'StoreController::index');


route('GET',    'customers', 
'CustomerController::index');

route('GET',    'customers/{customer_id}', 
'CustomerController::show');

route('POST',   'customers', 
'CustomerController::create');

route('PUT',    'customers/{customer_id}', 
'CustomerController::update');


route('GET',    'reservations', 
'ReservationController::index');

route('GET',    'reservations/{reservation_id}', 
'ReservationController::show');

route('POST',   'reservations', 
'ReservationController::create');

route('PUT',    'reservations/{reservation_id}', 
'ReservationController::update');

route('DELETE', 'reservations/{reservation_id}', 
'ReservationController::remove');


http_response_code(404);