<?php

class ReservationController{

    static function index($get, $post){
        return DB::select('SELECT * FROM `reservations`', []);
    }

    static function show($get, $post)
    {
        return DB::selectCascade('reservations', $get['reservation_id']);
        // return DB::select('SELECT * FROM `reservations` WHERE `id` = :reservation_id', $get);
    }

    static function create($get, $post)
    {
        if (DB::insert('reservations', $post)) {
            http_response_code(201);
            return 'success';
        } else {
            http_response_code(406);
            return 'failed';
        }
    }

    static function update($get, $post)
    {
        $post->id = $get['reservation_id'];
        if (DB::update('reservations', $post)) {
            http_response_code(202);
            return 'success';
        } else {
            http_response_code(406);
            return 'failed';
        }
    }

    static function delete($get, $post)
    {
        if (DB::delete('reservations', $get['reservation_id'])) {
            http_response_code(202);
            return 'success';
        } else {
            http_response_code(406);
            return 'failed';
        }
    }

}