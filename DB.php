<?php

require_once 'settings.php';

class DB
{
    private static $instance = null;

    private static $excludeOnUpdate = ['id', 'created_at', 'updated_at'];

    private $c;

    private function __construct()
    {
        try {
            $this->c = new PDO("mysql:host=".DB_SERVER.";dbname=".DB_NAME.";charset=utf8", DB_USERNAME, DB_PASSWORD);
            $this->c->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (Exception $e){
            echo "<script>alert('configure your database settings in DB.php')</script>";
            exit();
        }
    }

    public function getConnection(){
        return $this->c;
    }

    public static function getInstance()
    {
        if(self::$instance == null){
            self::$instance = new DB();
        }
        return self::$instance;
    }

    public static function selectCascade($table, $id){
        $obj = DB::select("SELECT * FROM `$table` WHERE `id` = :id", ['id' => $id]);
        if(count($obj) == 0) return $id;
        $obj = $obj[0];
        foreach($obj as $key => $value){
            if(DB::endsWith($key, '_id')){
                $t = explode('_', substr($key, 0, strlen($key) - 3));
                $t = $t[count($t) - 1];
                unset($obj[$key]);
                $obj[substr($key, 0, strlen($key) - 3)] = DB::selectCascade($t . 's', $value);
            }
        }
        return $obj;
    }

    public static function select($query, $params)
    {
        $stmt = self::prepare($query);
        foreach($params as $key => $value){
            if(strpos($query, ":$key") === false) continue;
            if(is_numeric($value)){
                $stmt->bindValue($key, $value, PDO::PARAM_INT);
            } else {
                $stmt->bindValue($key, $value, PDO::PARAM_STR);
            }
        }
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        return $stmt->fetchAll();
    }

    public static function update($table, $obj)
    {
        try{
            $qry = "UPDATE `$table` SET ";
            foreach($obj as $key => $value){
                if(self::skip($key)) continue;
                $qry .= "`$key` = :$key, ";
            }
            $qry = substr($qry, 0, strlen($qry) - 2);
            $qry .= " WHERE `id` = :id";
            $stmt = self::prepare($qry);
            $stmt->bindValue(":id", $obj->id);
            foreach($obj as $key => $value){
                if(self::skip($key)) continue;
                $stmt->bindValue(":$key", $value);
            }
            $stmt->execute();
            return true;
        } catch (Exception $e) {
            error_log($e->getMessage(), 0);
        }
        return false;
    }

    public static function insert($table, $obj)
    {
        try{
            $qry = "INSERT INTO `$table` (`" . join("`, `", self::stripColumns(array_keys((array) $obj))) . "`) VALUES (:" . join(", :", self::stripColumns(array_keys((array) $obj))) . ")";
            $stmt = self::completeQuery($qry, $obj);
            $stmt->execute();
            return true;
        } catch (Exception $e){
            error_log($e->getMessage(), 0);
        }
        return false;
    }

    public static function delete($table, $id)
    {
        try{
            $qry = "DELETE FROM `$table` WHERE `id` = :id";
            $stmt = self::prepare($qry);
            $stmt->bindValue(":id", $id);
            $stmt->execute();
            return true;
        } catch (Exception $e) {
            error_log($e->getMessage(), 0);
        }
        return false;
    }

    private static function completeQuery($qry, $params)
    {
        $stmt = self::prepare($qry);
        foreach($params as $key => $value){
            $stmt->bindValue(":$key", $value);
        }
        return $stmt;
    }

    public static function prepare($query)
    {
        return self::getInstance()->c->prepare($query);
    }

    public static function lastId()
    {
        return self::getInstance()->c->lastInsertId();
    }

    public static function close()
    {
        self::getInstance()->c = null;
        self::$instance = null;
    }

    private static function stripColumns($src)
    {
        foreach(self::$excludeOnUpdate as $key => $value){
            unset($src[$value]);
        }
        return $src;
    }

    private static function skip($var)
    {
        return array_search($var, self::$excludeOnUpdate) !== false;
    }

    private static function endsWith($string, $endString)
    {
        $len = strlen($endString);
        if ($len == 0) {
            return true;
        }
        return (substr($string, -$len) === $endString);
    } 
}
