<?php

session_start();
header('Content-Type: text/json; charset=utf-8');

function checkToken()
{
    try {
        $token = $_GET['token'];
        $user = json_decode(@file_get_contents("https://epic.clow.nl/token/check/" . $token));
        if (!isset($user->id)) {
            echo '{"message":"No user found for this token"}';
            die();
        }
        return $user;
    } catch (Exception $e) {
        echo '{"message":"No user found for this token"}';
        die();
    }
}

function getActiveUser($conn)
{
    $token = checkToken();
    $qry = "SELECT * FROM `users` WHERE `number` = :num";
    $stmt = $conn->prepare($qry);
    $stmt->bindParam(':num', $token->id, PDO::PARAM_STR);
    $stmt->execute();
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    return $stmt->fetch();
}

function route($method, $uri, $function)
{
    if($method != $_SERVER['REQUEST_METHOD']) return false;

    $targetUri = explode("/", $uri);
    $actualUri = explode("/", extractUri($_SERVER['REQUEST_URI']));

    if(count($targetUri) != count($actualUri)) return false;

    $get = $_GET;

    for ($i = 0; $i < count($actualUri); $i++){
        if(strpos($targetUri[$i], "{") !== false){
            $get[str_replace(['{', '}'], '', $targetUri[$i])] = $actualUri[$i];
        } else if($targetUri[$i] != $actualUri[$i]) {
            return false;
        }
    }
    $post = json_decode(stripslashes(file_get_contents("php://input")));
    $response = call_user_func_array($function, [$get, $post]);
    $encoded = json_encode($response);
    if(!$encoded){
        echo json_last_error_msg();
    } else {
        echo $encoded;
    }
    die();
}


function extractUri($url)
{
    $url = explode('/v1/', explode('?', $url)[0]);
    return isset($url[1]) ? $url[1] : "";
}

function message($str)
{
    return ['message' => $str];
}

function status($code)
{
    http_response_code($code);
    return message("failed");
}